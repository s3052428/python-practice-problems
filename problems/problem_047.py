# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) < 6 or len(password) > 12:
        return False
        
    if password.count("$") > 0 or password.count("!") > 0 or password.count('@') > 0:
        count_digit = 0
        upper_case = 0
        lower_case = 0
  
        for char in password: 
            if char.isdigit():
                count_digit += 1
            elif char.isupper():
                upper_case += 1
            elif char.islower():
                lower_case += 1
    
        if (count_digit < 1 and upper_case < 1 and lower_case < 1):
            return False
        
        return True  # If password passes all conditions
    else:
        return False



print(check_password("Letmein1@"))




