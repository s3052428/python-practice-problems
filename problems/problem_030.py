# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):

    if len(values) == 1 or not len(values):
        return None
    new_list = sorted(values)

    second_largest = new_list[0]
    largest = new_list[0]

    for num in new_list:
        if num >= largest:
            second_largest = largest
            largest = num

    return second_largest


print(find_second_largest([1, 3, 1, 10, 5, 2, 8, 4, 9, 6]))
print(find_second_largest([5]))
