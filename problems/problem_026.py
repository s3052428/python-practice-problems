# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if not len(values):
        return None
    avr = sum(values) / len(values)
    if avr >= 90:
        return "A"
    elif avr >= 80:
        return "B"
    elif avr >= 70:
        return "C"
    elif avr >= 60:
        return "D"
    else:
        return "F"


list_of_grades = [55, 55, 85, 100, 25]
print(sum(list_of_grades) / len(list_of_grades))
print(calculate_grade(list_of_grades))
