# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if not len(values):
        return None
    return sum(values) / len(values)


list1 = [4, 6, 8]
list2 = []
print(calculate_average(list1))
print(calculate_average(list2))
