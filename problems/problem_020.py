# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


def has_quorum(attendees_list, members_list):
    print(len(members_list)/2)
    return len(attendees_list) >= len(members_list)/2


list1 = ["matt", "noah"]
list2 = ["matt", "noah", "asdf", "asfs"]
list3 = ["matt", "noah", "asdf", "asfs", "iweo"]
print(has_quorum(list1, list2))
print(has_quorum(list1, list3))
