# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if not len(values):
        return None
    return sum(values)


list1 = [4, 6, 8]
list2 = []
print(calculate_sum(list1))
print(calculate_sum(list2))
