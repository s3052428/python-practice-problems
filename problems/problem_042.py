# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    new_list = []

    for i1, i2 in zip(list1, list2):   # zip(list1, list2) = (100, 10) - (200, 1) - (300, 180)
        # in the first iteration, i1 = 200, i2 = 1
        sum = i1 + i2 # sum = 201
        new_list.append(sum)  # new_list = [110, 201]

    return new_list

print(pairwise_add([100, 200, 300], [10,   1, 180]))
