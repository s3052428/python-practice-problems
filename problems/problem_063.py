# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem


def shift_letters(string):
    alpha = "abcdefghijklmnopqrstuvwxyz"
    alpha_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    shifted_string = ""
    for char in string:

        if char.islower():
            old_idx = alpha.index(char)
            new_idx = (old_idx + 1) % 26
            shifted_string += alpha[new_idx]
        else:
            old_idx = alpha_upper.index(char)
            new_idx = (old_idx + 1) % 26
            shifted_string += alpha_upper[new_idx]

    return shifted_string


print(shift_letters("import"))
