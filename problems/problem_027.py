# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if not len(values):
        return None
    return max(values)


list1 = []
print(max_in_list(list1))
